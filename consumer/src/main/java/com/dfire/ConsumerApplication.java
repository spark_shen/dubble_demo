package com.dfire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
@EnableAutoConfiguration
@ImportResource(locations = "classpath:dubbo/dubbo-consumer.xml")
@SpringBootApplication
public class ConsumerApplication {

//	@Autowired
//	private DemoService demoService;

	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
//        System.out.println(demoService.sayHello("world"));  
	}
}
