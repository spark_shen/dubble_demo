package com.dfire.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.dfire.service.DemoService;
import com.dfire.vo.Student;

/**
 *
 * @author xiaosuda
 * @date 2017/12/14
 */
@Service("demoServiceImpl")
public class DemoServiceImpl implements DemoService {
	@Override
	public String sayHello(String name) {
		Connection connection = null;
		PreparedStatement prepareStatement = null;
		ResultSet rs = null;

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		String salute = "";
		try {
			// 加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			// 获取连接
			String url = "jdbc:mysql://mysql.mysql:3306/alllink?statementInterceptors=brave.mysql.TracingStatementInterceptor";
			String user = "root";
			String password = "Abcd1234";
			connection = DriverManager.getConnection(url, user, password);
			// 获取statement，preparedStatement
			String sql = "select * from hello where id=?";
			prepareStatement = connection.prepareStatement(sql);
			// 设置参数
			prepareStatement.setLong(1, 1l);
			// 执行查询
			rs = prepareStatement.executeQuery();
			// 处理结果集
			while (rs.next()) {
				salute = rs.getString("salute");
				System.out.println(salute);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				// 关闭连接，释放资源
				if (rs != null) {
					rs.close();
				}
				if (prepareStatement != null) {
					prepareStatement.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return "你说：" + salute + "\\" + name;
	}

	@Override
	public Map<String, Student> getAllStudents(boolean isError) {
		Random r = new Random();
		Map<String, Student> res = new HashMap<>(10);
		for (int i = 0; i < 100000; i++) {
			Student stu = new Student();
			stu.setAge(r.nextInt(10) + 10);
			stu.setName("name:" + i);
			stu.setSex("男");
			res.put(stu.getName(), stu);
		}
		if(isError) {
			throw new RuntimeException("'Get-all-students' goes wrong");
		}
		return res;
	}
}
